import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore as createStoreInternal,
} from 'redux'
import {location, routerEnhancer, routerMiddleware} from './routes'
import {reports} from './reducers'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const reducers = {location, reports}

export default function createStore(...middlewares) {
  return createStoreInternal(
    combineReducers(reducers),
    composeEnhancers(
      routerEnhancer,
      applyMiddleware(routerMiddleware, ...middlewares)
    ),
  )
}