import MockData from './data/mock-data.json'
import {
  REPORT_CREATE_ERROR,
  REPORT_CREATE_REQUEST,
  REPORT_CREATE_SUCCESS,
  REPORT_DELETE_ERROR,
  REPORT_DELETE_REQUEST,
  REPORT_DELETE_SUCCESS,
} from './actions/reports'
import {complement, filter, map, propEq} from 'ramda'

const initialState = {
  members: MockData,
  creating: {},
}

export function reports(state = initialState, {type, ...action}) {
  switch (type) {
    case REPORT_DELETE_REQUEST: {
      return applyReportDeleteRequest(state, action.reportName)
    }

    case REPORT_DELETE_ERROR: {
      return applyReportDeleteError(state, action.reportName, action.error)
    }

    case REPORT_DELETE_SUCCESS: {
      return applyReportDeleteSuccess(state, action.reportName)
    }

    case REPORT_CREATE_REQUEST: {
      return applyReportCreateRequest(state, action.params)
    }

    case REPORT_CREATE_ERROR: {
      return applyReportCreateError(state, action.error)
    }

    case REPORT_CREATE_SUCCESS: {
      return applyReportCreateSuccess(state, action.params)
    }

    default: {
      return state
    }
  }
}

function applyReportDeleteRequest({members, ...state}, reportName) {
  return {members: map(setDeleting(reportName), members), ...state}
}

function setDeleting(reportName) {
  return report => reportName === report.name ?
    {...report, isDeleting: true} : report
}

function applyReportDeleteError({members, ...state}, reportName, error) {
  return {members: map(setError(reportName, error), members), ...state}
}

function setError(reportName, deleteError) {
  return report => reportName === report.name ?
    {...report, isDeleting: false, deleteError} : report
}

function applyReportDeleteSuccess({members, ...state}, reportName) {
  return {members: filter(reportDoesNotHaveName(reportName), members), ...state}
}

function reportDoesNotHaveName(name) {
  return complement(propEq('name', name))
}

function applyReportCreateRequest(state, params) {
  return {...state, creating: params}
}

function applyReportCreateError(state, error) {
  return {...state, creating: {...state.creating, error}}
}

function applyReportCreateSuccess(state, params) {
  return {members: state.members.concat(params), creating: {}}
}