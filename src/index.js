import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './components/App'
import * as serviceWorker from './serviceWorker'
import createStore from './store'
import {Provider} from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import runSagas from './sagas'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(sagaMiddleware)


ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root'),
)

runSagas(sagaMiddleware)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
