export const REPORT_DELETE = 'REPORT_DELETE'
export const REPORT_DELETE_REQUEST = 'REPORT_DELETE_REQUEST'
export const REPORT_DELETE_ERROR = 'REPORT_DELETE_ERROR'
export const REPORT_DELETE_SUCCESS = 'REPORT_DELETE_SUCCESS'
export const REPORT_CREATE = 'REPORT_CREATE'
export const REPORT_CREATE_REQUEST = 'REPORT_CREATE_REQUEST'
export const REPORT_CREATE_ERROR = 'REPORT_CREATE_ERROR'
export const REPORT_CREATE_SUCCESS = 'REPORT_CREATE_SUCCESS'

export function reportDelete(reportName) {
  return {type: REPORT_DELETE, reportName}
}

export function reportDeleteRequest(reportName) {
  return {type: REPORT_DELETE_REQUEST, reportName}
}

export function reportDeleteError(reportName, error) {
  return {type: REPORT_DELETE_ERROR, reportName, error}
}

export function reportDeleteSuccess(reportName) {
  return {type: REPORT_DELETE_SUCCESS, reportName}
}

export function reportCreate(params) {
  return {type: REPORT_CREATE, params}
}

export function reportCreateRequest(params) {
  return {type: REPORT_CREATE_REQUEST, params}
}

export function reportCreateError(error) {
  return {type: REPORT_CREATE_ERROR, error}
}

export function reportCreateSuccess(params) {
  return {type: REPORT_CREATE_SUCCESS, params}
}