import {connectRoutes} from 'redux-first-router'
import queryString from 'query-string'

export const ROOT = 'root'

const routesMap = {
  [ROOT]: '/',
}

export const {
  reducer: location,
  middleware: routerMiddleware,
  enhancer: routerEnhancer,
} = connectRoutes(routesMap, {querySerializer: queryString})