import React from 'react'
import {connect} from 'react-redux'
import Main from '../components/FilterMenu'
import location from '../actions/location'
import {
  filtersFromLocationQuery,
  filterTypesFromReports,
} from '../helpers/reports'

class FilterMenu extends React.Component {
  render() {
    const {filters, filterOptions} = this.props
    return <Main {...{filters, filterOptions}} onChange={this.handleChange}/>
  }

  handleChange = ({target: {value}}) => {
    const {location, locationType, query} = this.props


    location(locationType, {}, {
      ...query,
      filters: value.length ? JSON.stringify(value) : undefined,
    })
  }
}

function mapStateToProps({reports: {members}, location: {type, query}}) {
  const filterOptions = filterTypesFromReports(members)

  return {
    locationType: type,
    query,
    filters: filtersFromLocationQuery(query),
    filterOptions,
  }
}

const mapDispatchToProps = {location}

export default connect(mapStateToProps, mapDispatchToProps)(FilterMenu)