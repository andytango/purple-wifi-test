import React from 'react'
import Main from '../components/ReportCreateForm'
import {reportCreate} from '../actions/reports'
import {connect} from 'react-redux'
import location from '../actions/location'

class CreateReportForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: 'Untitled Report',
      type: 'Visitors',
      chartType: 'bar',
      filterTypes: [],
      frequency: 'monthly',
      active: false,
    }
  }

  render() {

    if(this.shouldRender()) {
      return this.renderForm()
    }

    return null
  }

  shouldRender() {
    const {locationQuery} = this.props
    return locationQuery && (
      locationQuery.createForm === true ||
      locationQuery.createForm === 'true'
    )
  }

  renderForm() {
    const params = this.state
    const {error} = this.props

    return <Main
      {...{params, error}}
      onChange={this.handleChange}
      onSubmit={this.handleSubmit}
      onCancel={this.handleCancel}
    />
  }

  handleChange = (k, v) => {
    this.setState({[k]: v})
  }

  handleSubmit = () => {
    this.props.reportCreate(this.state)
  }

  handleCancel = () => {
    const {location, locationType, locationQuery} = this.props
    location(locationType, {}, {...locationQuery, createForm: undefined})
  }
}

function mapStateToProps({location: {type, query}, reports}) {
  return {
    locationType: type,
    locationQuery: query,
    error: reports.creating.error
  }
}

const mapDispatchToProps = {location, reportCreate}

export default connect(mapStateToProps, mapDispatchToProps)(CreateReportForm)