import React from 'react'
import {connect} from 'react-redux'
import Main from '../components/SortButton'
import location from '../actions/location'
import {sortDescendingFromLocationQuery} from '../helpers/reports'

class SortButton extends React.Component {
  render() {
    const {sortDescending} = this.props
    return <Main
      sortDescending={sortDescending}
      onSortChange={this.handleSortChange}
    />
  }

  handleSortChange = () => {
    const {location, locationType, sortDescending} = this.props

    location(locationType, {}, {sortDescending: !sortDescending || undefined})
  }
}

function mapStateToProps({location: {type, query}}) {
  return {
    locationType: type,
    sortDescending: sortDescendingFromLocationQuery(query),
  }
}

const mapDispatchToProps = {location}

export default connect(mapStateToProps, mapDispatchToProps)(SortButton)