import React from 'react'
import {connect} from 'react-redux'
import Main from '../components/ReportCreateButton'
import location from '../actions/location'

class CreateReportButton extends React.Component {
  render() {
    return <Main onClick={this.handleClick}/>
  }

  handleClick = () => {
    const {location, locationType, locationQuery} = this.props

    location(locationType, {}, {...locationQuery, createForm: true})
  }
}

function mapStateToProps({location: {type, query}}) {
  return {locationType: type, locationQuery: query}
}

const mapDispatchToProps = {location}

export default connect(mapStateToProps, mapDispatchToProps)(CreateReportButton)