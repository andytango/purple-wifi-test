import React from 'react'
import {connect} from 'react-redux'
import Main from '../components/ReportsList'
import {
  filtersFromLocationQuery, getReports,
  sortDescendingFromLocationQuery,
} from '../helpers/reports'

const ReportsList = (props) => <Main {...props}/>


function mapStateToProps({reports: {members}, location: {query}}) {
  const sortDescending = sortDescendingFromLocationQuery(query)
  const filters = filtersFromLocationQuery(query)

  return {
    reports: getReports(members, sortDescending, filters),
  }
}

export default connect(mapStateToProps, null)(ReportsList)