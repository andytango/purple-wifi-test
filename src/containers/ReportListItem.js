import React from 'react'
import {connect} from 'react-redux'
import Main from '../components/ReportListItem'
import {reportDelete} from '../actions/reports'
import {isReportDeleting} from '../helpers/reports'

const ReportListItem = (props) => <Main {...props}/>


function mapStateToProps({reports: {members}}, {report: {name}}) {
  return {
    isDeleting: isReportDeleting(members, name),
  }
}

const mapDispatchToProps = {reportDelete}

export default connect(mapStateToProps, mapDispatchToProps)(ReportListItem)