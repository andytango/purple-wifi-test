import {delay} from 'redux-saga'
import {call} from 'redux-saga/effects'

const SUCCESS_PROBABILITY = 0.8

export function* fakeNetworkDelay() {
  yield call(delay, getFakeNetworkDelay())
}

export function fakeNetworkResponse(onSuccess, onError) {
  if(getFakeSuccessState()) {
    return onSuccess()
  }

  return onError()
}

function getFakeSuccessState() {
  return Math.random() < SUCCESS_PROBABILITY
}

function getFakeNetworkDelay() {
  return 200 + Math.random() * 800
}