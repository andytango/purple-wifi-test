import {
  complement,
  filter,
  find,
  flatten,
  identity,
  intersection,
  isEmpty,
  map,
  pipe,
  prop,
  propEq,
  sortBy,
  uniq,
} from 'ramda'

export function sortDescendingFromLocationQuery(query) {
  return query && (
    query.sortDescending === true ||
    query.sortDescending === 'true'
  )
}

export function getReports(reports, desc, filters) {
  return applySort(applyFilters(reports, filters), desc)
}

export function filtersFromLocationQuery(query) {
  return query ? parseJsonArrayFromQueryValue(query.filters) : []
}

export function filterTypesFromReports(reports) {
  return sortBy(identity, uniq(flatten(map(prop('filterTypes'), reports))))
}

export function isReportDeleting(reports, name) {
  return findReportByName(reports, name).isDeleting
}

function findReportByName(reports, name) {
  return find(propEq('name', name), reports)
}

function applyFilters(reports, filters) {
  return (filters.length > 0) ?
    filter(filterTypesOverlap(filters), reports) :
    reports
}

const isNotEmpty = complement(isEmpty)

const filterTypesProp = prop('filterTypes')

const filterTypesOverlap = (list) =>
  pipe(filterTypesProp, intersection(list), isNotEmpty)

function applySort(reports, desc) {
  return desc ? sortByName(reports).reverse() : sortByName(reports)
}

const sortByName = sortBy(prop('name'))

function parseJsonArrayFromQueryValue(s) {
  try {
    return checkArray(JSON.parse(s))
  } catch {
    return []
  }
}

function checkArray(arr) {
  if (Array.isArray(arr)) {
    return arr
  }

  throw new Error()
}
