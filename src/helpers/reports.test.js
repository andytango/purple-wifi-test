import {sortDescendingFromLocationQuery, getReports} from './reports'

describe('sortDescendingFromLocationQuery', () => {
  const nonObjectTypes = [undefined, null, [], '', 's', 0, 1]
  const falseTypes = nonObjectTypes.concat({})

  it('returns false with non-object query', () => {
    nonObjectTypes.forEach(val => {
      expect(sortDescendingFromLocationQuery(val)).toBeFalsy()
    })

  })

  it('returns false with other types ', () => {
    falseTypes.forEach(val => {
      expect(sortDescendingFromLocationQuery(val)).toBeFalsy()
    })
  })

  it('returns true with a string value', () => {
    expect(sortDescendingFromLocationQuery({
      sortDescending: 'true',
    })).toBeTruthy()
  })

  it('returns true with a boolean value', () => {
    expect(sortDescendingFromLocationQuery({
      sortDescending: true,
    })).toBeTruthy()
  })

})

describe('getReports', () => {
  const testData = [
    {name: 'b', filterTypes: ['a'] },
    {name: 'a', filterTypes: ['b'] },
    {name: 'c', filterTypes: ['b'] },
  ]

  const sortedResult = ['a', 'b', 'c']
  const reverseSortedResult = sortedResult.concat().reverse()

  it("sorts ascending by default", () => {
    const reports = getReports(testData, undefined, [])

    sortedResult.forEach((val, i) => {
      expect(reports[i].name).toEqual(val)
    })
  })

  it("sorts descending when desc is true", () => {
    const reports = getReports(testData, true, [])

    reverseSortedResult.forEach((val, i) => {
      expect(reports[i].name).toEqual(val)
    })
  })

  it("does not filter when filters argument is empty", () => {
    expect(getReports(testData, true, []).length).toEqual(3)
    reverseSortedResult.forEach((val, i) => {
    })
  })

  it("filters when given a non-empty filters argument", () => {
    expect(getReports(testData, true, ['a']).length).toEqual(1)
    reverseSortedResult.forEach((val, i) => {

    })
  })

  it("returns the union of given filters", () => {
    expect(getReports(testData, true, ['a', 'b']).length).toEqual(3)
  })

})