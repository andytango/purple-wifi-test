import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import createStore from '../store'
import {Provider} from 'react-redux'

it('renders without crashing', () => {
  const div = document.createElement('div')
  const store = createStore()

  ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>,
    div,
  )
  ReactDOM.unmountComponentAtNode(div)
})
