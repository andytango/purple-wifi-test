import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import ReportsListItem from '../containers/ReportListItem'
import Divider from '@material-ui/core/Divider'
import SortButton from '../containers/SortButton'
import FilterMenu from '../containers/FilterMenu'
import Grid from '@material-ui/core/Grid'
import ReportCreateButton from '../containers/ReportCreateButton'
import ReportCreateForm from '../containers/ReportCreateForm'

const styles = (theme) => ({
  root: {
    maxWidth: '768px',
    margin: 'auto',
  },
  controls: {
    margin: `${theme.spacing.unit}px 0`,
  },
  sortButton: {
    marginTop: '20px',
  },
  createButton: {
    marginTop: '20px',
    textAlign: 'right'
  },
})

const Base = (
  {classes: {root, controls, sortButton, createButton}, reports},
) => (
  <div className={root}>
    <div className={controls}>
      <Grid container spacing={16}>
        <Grid item xs={4} className={sortButton}><SortButton/></Grid>
        <Grid item xs={4}><FilterMenu/></Grid>
        <Grid item xs={4} className={createButton}><ReportCreateButton/></Grid>
      </Grid>
    </div>
    <Divider/>
    <div>{reports.map(toListItem)}</div>
    <ReportCreateForm />
  </div>
)

function toListItem(report, key) {
  return <ReportsListItem {...{report, key}} />
}

export default withStyles(styles)(Base)