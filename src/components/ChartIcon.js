import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import PieChartIcon from '@material-ui/icons/PieChart'
import ColumnChartIcon from '@material-ui/icons/BarChart'

const ChartIcon = ({ type, ...props }) =>
  React.createElement(chartIcons[type], props)

const styles = {
  root: {
    display: 'inline-block',
    transform: 'rotate(90deg)',
  },
}

const BarChartIconBase = ({classes, ...props}) => (
  <div className={classes.root}>
    <ColumnChartIcon {...props}/>
  </div>
)

const BarChartIcon = withStyles(styles)(BarChartIconBase)

const chartIcons = {
  bar: BarChartIcon,
  pie: PieChartIcon,
  column: ColumnChartIcon,
}

export default ChartIcon
