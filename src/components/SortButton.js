import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import Button from '@material-ui/core/Button'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownwardOutlined'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpwardOutlined'

const styles = (theme) => ({
  sortIcon: {
    marginLeft: theme.spacing.unit,
  },
})

const Base = ({classes, sortDescending, onSortChange}) => (
  <Button variant="outlined" onClick={onSortChange}>
    Sort by Name
    <SortIcon {...{sortDescending}} className={classes.sortIcon}/>
  </Button>
)

const SortIcon = ({sortDescending, ...props}) => (
  sortDescending ?
    <ArrowDownwardIcon {...props} /> :
    <ArrowUpwardIcon {...props}/>
)

export default withStyles(styles)(Base)