import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import Input from '@material-ui/core/Input'
import MenuItem from '@material-ui/core/MenuItem'
import Chip from '@material-ui/core/Chip'

const styles = (theme) => ({
  formControl: {
    margin: theme.spacing.unit,
    minWidth: '100%',
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing.unit / 4,
  },
})

const filterOptions = [
  'Gender', 'Age range', 'Location', 'Device type', 'Operating system'
]

const Base = ({classes, value, onChange}) => (
  <FormControl className={classes.formControl}>
    <InputLabel htmlFor="select-multiple">Filters</InputLabel>
    <Select
      multiple
      {...{value}}
      onChange={onChange}
      input={<Input id="select-multiple"/>}
      renderValue={selected => (
        <div className={classes.chips}>
          {selected.map(value => (
            <Chip key={value} label={value} className={classes.chip}/>
          ))}
        </div>
      )}
    >
      {filterOptions.map(toMenuItem)}
    </Select>
  </FormControl>
)

function toMenuItem(name) {
  return <MenuItem key={name} value={name}>{name}</MenuItem>
}

export default withStyles(styles)(Base)

