import React from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import DropdownMenu from '../common/DropdownMenu'

const TypeMenu = (props) => (
  <DropdownMenu label="Frequency" {...props}>
    <MenuItem value="monthly">Monthly</MenuItem>
    <MenuItem value="weekly">Weekly</MenuItem>
  </DropdownMenu>
)


export default TypeMenu