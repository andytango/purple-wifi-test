import React from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import DropdownMenu from '../common/DropdownMenu'

const TypeMenu = (props) => (
  <DropdownMenu label="Report Type" {...props}>
    <MenuItem value="Visitors">Visitors</MenuItem>
    <MenuItem value="Gender">Gender</MenuItem>
    <MenuItem value="Age range">Age range</MenuItem>
  </DropdownMenu>
)

export default TypeMenu