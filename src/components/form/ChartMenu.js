import React from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import DropdownMenu from '../common/DropdownMenu'

const ChartMenu = (props) => (
  <DropdownMenu {...props}>
    <MenuItem value="bar">Bar</MenuItem>
    <MenuItem value="column">Column</MenuItem>
    <MenuItem value="pie">Pie</MenuItem>
  </DropdownMenu>
)


export default ChartMenu