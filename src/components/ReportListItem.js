import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import ChartIcon from './ChartIcon'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Chip from '@material-ui/core/Chip'
import Divider from '@material-ui/core/Divider'
import FilterChips from './FilterChips'
import InlineTypography from './common/InlineTypography'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import ErrorTypography from './common/ErrorTypography'

const styles = theme => ({
  root: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  deleting: {
    opacity: 0.32,
  },
  chartType: {
    fontSize: '72px',
  },
  details: {
    margin: `${theme.spacing.unit}px 0`,
  },
})

const Base = (
  {
    classes,
    report: {
      name,
      type,
      chartType,
      frequency,
      active,
      filterTypes,
      isDeleting,
      deleteError,
    },
    reportDelete,
  },
) => (
  <Paper className={getRootClassName(isDeleting, classes)}>
    <Grid container spacing={16}>
      <Grid item>
        <ChartIcon
          type={chartType}
          className={classes.chartType}
          color={isDeleting ? 'disabled' : 'primary'}
        />
      </Grid>
      <Grid item xs={9}>
        <Typography variant="subtitle1" gutterBottom>{name}</Typography>
        <Divider/>
        <div className={classes.details}>
          <InlineTypography color="textSecondary">Type: </InlineTypography>
          <InlineTypography>{type}</InlineTypography>
        </div>
        <div className={classes.details}>
          <InlineTypography color="textSecondary">Frequency: </InlineTypography>
          <InlineTypography>{frequency}</InlineTypography>
        </div>
        <div className={classes.details}>
          <InlineTypography color="textSecondary">
            Filter Types: &nbsp;
          </InlineTypography>
          <span>
            <FilterChips {...{filterTypes}} />
          </span>
        </div>
        {deleteError && <ErrorTypography>
          The following error occurred deleting this report:{deleteError}
        </ErrorTypography>}
      </Grid>
      <Grid item xs={1}>
        <Chip label={active ? 'Active' : 'Inactive'}
              className={classes.chip}
              color={active ? 'primary' : 'default'}
        />
        <p>
          <IconButton
            onClick={() => reportDelete(name)}
            disabled={isDeleting}
          >
            <DeleteIcon/>
          </IconButton>
        </p>
      </Grid>
    </Grid>
  </Paper>
)

function getRootClassName(isDeleting, classes) {
  return isDeleting ? `${classes.root} ${classes.deleting}` : classes.root
}

export default withStyles(styles)(Base)