import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import Typography from '@material-ui/core/Typography'

const InlineTypography = withStyles({
  root: {
    display: 'inline',
  },
})(
  ({classes, ...props}) => <Typography className={classes.root} {...props} />,
)


export default InlineTypography