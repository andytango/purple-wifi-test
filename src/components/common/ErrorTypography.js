import React from 'react'
import Typography from '@material-ui/core/Typography'
import red from '@material-ui/core/colors/red'

const ErrorTypography = ({error, ...props}) => (
  <Typography style={{color: red[400]}} {...props} />
)

export default ErrorTypography