import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import * as ReactDOM from 'react-dom'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 90,
  },
})


class Base extends React.Component {
  constructor(props) {
    super(props)
    this.state = {labelWidth: 0}
  }

  componentDidMount() {
    this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
    })
  }

  render() {
    const {classes, label, children} = this.props
    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel
          ref={ref => {
            this.InputLabelRef = ref
          }}
        >
          {label}
        </InputLabel>
        <Select
          value={this.props.value}
          onChange={this.handleChange}
          input={
            <OutlinedInput
              labelWidth={this.state.labelWidth}
            />
          }
        >
          {children}

        </Select>
      </FormControl>
    )
  }

  handleChange = (e) => {
    this.props.onChange(e.target.value)
  }
}

export default withStyles(styles)(Base)