import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import ChartMenu from './form/ChartMenu'
import TextField from '@material-ui/core/TextField'
import TypeMenu from './form/TypeMenu'
import FrequencyMenu from './form/FrequencyMenu'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FilterTypesMenu from './form/FilterTypesMenu'
import Button from '@material-ui/core/Button'
import ErrorTypography from './common/ErrorTypography'

const styles = theme => ({
  root: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  actions: {
    textAlign: 'right',
  },
  button: {
    margin: theme.spacing.unit,
  },
})

const Base = (
  {
    classes,
    params: {
      name,
      type,
      chartType,
      frequency,
      active,
      filterTypes,
    },
    onChange,
    onSubmit,
    onCancel,
    error,
  },
) => (
  <Paper className={classes.root}>
    <Grid container spacing={16}>
      <Grid item>
        <ChartMenu
          value={chartType}
          onChange={(v) => onChange('chartType', v)}
        />
      </Grid>
      <Grid item xs={8}>
        <TextField
          value={name}
          onChange={(e) => onChange('name', e.target.value)}
          label="Name"
          style={{margin: 8}}
          placeholder="Enter a name"
          fullWidth
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Divider/>
        <div className={classes.details}>
          <TypeMenu
            value={type}
            onChange={(v) => onChange('type', v)}
          />
          <FrequencyMenu
            value={frequency}
            onChange={(v) => onChange('frequency', v)}
          />
        </div>
        <div className={classes.details}>
          <FilterTypesMenu
            value={filterTypes}
            onChange={(e) => onChange('filterTypes', e.target.value)}
          />
        </div>
      </Grid>
      <Grid item xs={1}>
        <FormControlLabel
          control={
            <Checkbox
              checked={active}
              onChange={(e) => onChange('active', e.target.checked)}
              color="primary"
            />
          }
          label="Active"
        />
      </Grid>
      {error && <Grid item xs={12}>
        <ErrorTypography>
          The following error occurred creating this report: {error}
        </ErrorTypography>
      </Grid>}
    </Grid>
    <div className={classes.actions}>
      <Button className={classes.button} variant="outlined" onClick={onCancel}>
        Cancel
      </Button>
      <Button className={classes.button} variant="outlined" onClick={onSubmit}>
        Submit
      </Button>
    </div>
  </Paper>
)


export default withStyles(styles)(Base)