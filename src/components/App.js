import React, {Component} from 'react'
import ReportsList from '../containers/ReportsList'

class App extends Component {
  render() {
    return (
      <div>
        <ReportsList />
      </div>
    );
  }
}

export default App;
