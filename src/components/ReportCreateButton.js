import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/icons/Create'

const styles = (theme) => ({
  icon: {
    marginLeft: theme.spacing.unit,
  },
})

const Base = ({classes, ...props}) => (
  <Button variant="outlined" {...props}>
    Create New Report
    <Icon className={classes.icon}/>
  </Button>
)

export default withStyles(styles)(Base)