import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import Chip from '@material-ui/core/Chip'
import InlineTypography from './common/InlineTypography'

const styles = (theme) => ({
  root: {
    margin: theme.spacing.unit,
  }
})

const FilterChips = ({ filterTypes }) => (
  filterTypes.length > 0 ?
    filterTypes.map((label, key) => <FilterChip {...{label, key}} />) :
    <InlineTypography>N/A</InlineTypography>
)

const FilterChipBase = ({ classes: {root}, label }) =>
  <Chip className={root} {...{label}} />

const FilterChip = withStyles(styles)(FilterChipBase)

export default FilterChips