import reportDelete from './sagas/reportDelete'
import reportCreate from './sagas/reportCreate'

const sagas = [
  reportDelete,
  reportCreate,
]

export default function runSagas(middleware) {
  sagas.forEach(runSagaWithMiddleware(middleware))
}

function runSagaWithMiddleware(middleware) {
  return saga => middleware.run(saga)
}