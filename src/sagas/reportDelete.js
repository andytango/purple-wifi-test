import {put, select, takeEvery} from 'redux-saga/effects'
import {
  REPORT_DELETE,
  reportDeleteError,
  reportDeleteRequest,
  reportDeleteSuccess,
} from '../actions/reports'
import {isReportDeleting} from '../helpers/reports'
import {fakeNetworkDelay, fakeNetworkResponse} from '../helpers/sagas'

export default function* reportDelete() {
  yield takeEvery(REPORT_DELETE, handleReportDelete)
}

function* handleReportDelete({reportName}) {
  const isDeleting = yield select(isReportDeletingFromState(reportName))

  if (!isDeleting) {
    yield put(reportDeleteRequest(reportName))
    yield fakeNetworkDelay()
    yield fakeNetworkResponse(
      () => put(reportDeleteSuccess(reportName)),
      () => put(reportDeleteError(reportName, 'Something went awry.'))
    )
  }
}

function isReportDeletingFromState(reportName) {
  return ({reports: {members}}) => {
    return isReportDeleting(members, reportName)
  }
}