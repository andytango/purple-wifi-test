import {put, select, takeEvery} from 'redux-saga/effects'
import {
  REPORT_CREATE,
  reportCreateError,
  reportCreateRequest,
  reportCreateSuccess,
} from '../actions/reports'
import {fakeNetworkDelay, fakeNetworkResponse} from '../helpers/sagas'

export default function* reportCreate() {
  yield takeEvery(REPORT_CREATE, handleReportCreate)
}

function* handleReportCreate({params}) {
  const isCreatingReport = yield select(isCreatingReportFromState(params.name))

  if(!isCreatingReport) {
    yield put(reportCreateRequest(params))
    yield fakeNetworkDelay()
    yield fakeNetworkResponse(
      () => put(reportCreateSuccess(params)),
      () => put(reportCreateError('Something went awry.'))
    )
  }
}

function isCreatingReportFromState(query) {
  return ({ reports: {creating: {name}} }) => name === query
}